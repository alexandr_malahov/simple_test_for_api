import requests
import pytest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver



@pytest.fixture()
def data():
    return 'http://google.com'


@pytest.fixture()
def driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    return driver

def test_one(data):
    r = requests.get(data)
    print(r.status_code)
    assert r.status_code == 200


@pytest.mark.parametrize('url', ['http://onliner.com', 'http://yandex.ru'])
def test_two(url):
    r = requests.get(url)
    print(r.status_code)
    print(f'for {url} staus code is {r.status_code}')
    assert r.status_code == 200


def test_web(driver,data):
    driver.get(data)
    print('passed')
    driver.quit()
